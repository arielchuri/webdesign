// MadLib
var story = document.getElementById("story");
story.style.display = "none";
var submit = document.getElementById("submit");
var question = document.getElementById("question");
submit.onclick = function() {
  var word01 = document.getElementById("field01").value;
  document.getElementById("word01").innerHTML = word01;
  story.style.display = "block";
  question.style.display = "none";
}

// Calculator
//Make and store variables
var submit2 = document.getElementById("submit2"); // Get button
submit2.onclick = function() {
  var numberA = Number(document.getElementById("field02").value);
  var operator = document.getElementById("field03").value;
  var numberB = Number(document.getElementById("field04").value);
  calculate(numberA, operator, numberB);
}

function calculate(a, b, c) {
  if (b == "+"){
    var answer = a + c;
  } else if (b == "-"){
    var answer = a - c;
  } else if (b == "*") {
    var answer = a * c;
  } else if (b == "/"){
    var answer = a / c;
  }
  console.log("The answer to ",a,b,c," is ",answer);
  document.getElementById("answer").innerHTML = answer;
}

