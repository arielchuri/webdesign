# Syllabus
Parsons School of Design   
Communication Design Department   

**Web Design Basics**
PSAM 1028 | CRN 3066 | Section J   
Spring 2020   
Monday, 7-9:40pm   
Location 63 Fifth Ave., Room 417

Ariel Churi churia@newschool.edu   
[Google Drive Folder](https://drive.google.com/open?id=1dAshRw3-IIL4HRXM_qcx4nthvplHT8Ie)

## Course Description
*Web Design Basics* is designed to introduce students to programming as a creative medium-as a way of making and exploring. The coursework focuses on developing a vocabulary of interaction design principles which can then be applied across a range of platforms. Students are encouraged to experiment with various media, tools, and techniques, ultimately producing a portfolio of interactive and visual projects designed for the screen. An emphasis is placed on typography as it applies to a screen context, research-based problem solving and a "learning through making** approach to technical skill building. Historical and current interaction design precedents will be discussed.

## Learning Outcomes
1. Demonstrate knowledge of fundamental programming concepts
2. Develop several visual & interactive projects
3. Objectively present their design process and workflow
4. Integrate a variety of media elements into their projects
5. Problem solve with other students through reading other students’ code
6. Demonstrate the ability to research and learn unfamiliar technical topics.
7. Concept projects that use code in a creative way
8. Research historic and current design precedents to contextualize their work.
9. Evaluate how typography and its variables are applied to interactive systems to facilitate orientation, support usability and create consistency
   
## Final Grade Calculation
- In-class and Take-home Assignments    50%
- Final Project                         30%
- In-class Participation                20%

## Course Overview
### Weeks 1-6
The development environment, markup in general, and web development with HTML
and CSS
### Week 7
Midterm evaluation
### Weeks 8-10
Javascript
### Weeks 11-15
Advanced topics and final project
## Course Outline
### Week 1 -  Jan. 27 
- How Computers Work and Organizing your Files
- The Development Environment: Browser Web Developer Tools and Text Editors
- Firefox Developer Edition, BBEdit
- Markup languages and HTML. What it is and what it is not
- HTML Syntax and writing for computers
- Coding an HTML page
<!-- - Sophomore Work Collection -->
##### Assignment
- **_Recipe_** (due next session)
- **_Class Form_** (due next session)
### Week 2 - Feb. 3
- CSS and HTML
- How CSS works
- CSS selectors
##### Assignment
- **_Recipe Style_** (due next session)
### Week 3 - Feb. 10
- Color on the web
- Compressing folders with ZIP
- Pixels and em spaces
- The box model
- Commenting and Indenting
- CSS resets
##### Assignment
- **_Style Guide_** (due next session)
###  No class - Feb. 17
### Week 4 - Feb. 24
- Layout with CSS
- Padding and margins
- CSS Columns
- Flexbox
##### Assignment
- **_Recipe Style 2_** (due next session)
### Week 5 - Mar. 2
- Type on the web
- Grid Systems
- CSS Shapes
- CSS Positioning
##### Assignment
- **_Circle, Square, Triangle_** (due next session)
### Week 6 - Mar. 9
- Responsive design
- Media queries
##### Assignment
- **_Responsive Recipe_** (due next session)
### No class - Mar. 16
### Week 7 - Mar. 23
- Mid semester Review
- Links
- Linking to sections of the current page
#### Assignment
- **_Bestiary Page_** (due next session)
### Week 8 - Mar. 30
- CSS Animations
- CSS Gradients
- Alternate ways of using images
##### Assignment
- **_CSS Effects_**
### Week 9 - Apr. 6
- Introduction to Javascript
- Understanding the DOM
- Selecting objects in the DOM
- Javascript - Working with page events
##### Assignment
- **_Javascript Review_**
- **_MadLib_**
### Week 10 - Apr. 13
- Javascript functions
- Creating responsive navigation
##### Assignment
**_Final Project Proposal_** (due next session)
### Week 11 - Apr. 20
- Downloading and using Javascript libraries
    - jQuery
    - lightbox.js
##### Assignment
- **_Development Plan_** (due next session)
- **_Final 1_** (due next session)
### Week 12 - Apr. 27
- Using HTML Frameworks
##### Assignment
- **_Final 2_** (due next session)
### Week 13 - May. 4
##### Assignment
- **_Final_** (due in two sessions)
### Week 14 - May. 11
- Work session
### Week 15 - May. 12
- Review of final project
- Overview

## Assessable Tasks
#### Recipe
A recipe is a set of instructions that describes how to prepare or make something. Create a web page showing a recipe. Your recipe should not be a recipe for food. You could give instructions for performing a ritual, creating an object or a process. Your recipe must contain the following items:

- Title
- Main image
- Preparation time
- Amount of servings
- Description
- Unordered list of at least five ingredients
- Ordered list of at least five steps 
- At least one image

Keep the following in mind:
- Clean code
- Correct use of HTML codes
- Use Paragraph and Div tags to divide up different types of information.

    This assignment is designed to begin the process of familiarizing students with the development environment as well as basic HTML. Understanding how to properly organize files and the use of a plain text editor will be required. Additionally, students will have to research how to add an image to their HTML. This assignment will begin the process of understanding programming concepts and researching unfamiliar technical topics.

#### Recipe Style
Use the concepts we learned in class to add styling to your recipe. Please keep in mind the difference in purpose between HTML and CSS. Do not leave aspects of your design to the web browsers default settings. Take care to ensure careful use of type and hierarchy in your work.
Make design decisions regarding:
- Body copy size and spacing
- Header size and spacing
- Paragraphs

- Text styling such as bold, italic, links, etc.

<!-- #### Hypertext Narrative -->
<!-- Create a hypertext narrative using image and text that allows the user to create their own _choose your own adventure_ style story. Use at least 6 interconnect pages to allow the reader to progress to multiple potential endings. Each page should allow the user to make a decision that changes the course of the narrative. -->

<!-- #### Lasers & Feelings -->
<!-- Lasers & Feelings in an example of a one-page role playing game. Many people -->
<!-- have "hacked" the game to make it their own. Create a webpage that is your hack -->
<!-- of Lasers and feelings. Change the name of the game and some of the copy to give -->
<!-- it your own flavor.  -->

<!-- Produce a website no more than one or two screens tall that contains all of the rules -->
<!-- to play your version of Lasers & Feelings. -->

<!-- - [Lasers & Feelings](http://www.onesevendesign.com/laserfeelings/) -->
<!-- - [Muppet Babies RPG](http://origamigaming.com/index.php/2017/07/30/muppet-babies-rpg/)  -->
<!-- - [Facts & Innuendos](http://ashgamedesign.com/wp-content/uploads/2017/07/facts-innuendosv1.0.pdf) -->
<!-- - [Pisskids and Ratbags](http://www.drivethrurpg.com/product/212258/Pisskids-and-Ratbags) -->

#### Style Guide
Create a _Style Guide_ that can be used as a tool for making design decisions. Similar to a _type specimen_, this style guide shows how type can be used on the web. Using the provided examples as a guide, create an HTML/CSS document that shows a variety of layouts of body copy, justification styles, paragraphs, and headlines. Include a color palette, use of images and forms. Create two documents. One using a serif typeface and one for sans-serif.


<!--     The careful use of type is one of the challenges of a design education. The development of a personal style guide allows an artist or designer to develop a relationship with the presentation of type without a concern for content.   -->

#### Recipe Style 2
Edit your reipe document to use the material we covered in class. 
- Padding and margins
- Columns
- Flexbox


#### Circle Triangle Square
Create a page 3 screens tall. Each screen has a circle, an equilateral triangle,
and a square in a horizontal row. Choose a yellow, a red, and a blue to color
each shape a different color. Using only those three colors, assign the shapes a
different color on each screen. Change only the color of the shapes on each screen
and then adjust the spacing.

In each screen:

- the three shapes appear to be centered
- the three shapes appear to be the same distance apart
- the tree shapes appear to be on the same baseline

#### Responsive Recipe
Edit your recipe to be viewed across three different screen sizes.

#### Bestiary Page
Create a webpage that is a single page of a
[bestiary](https://en.wikipedia.org/wiki/Bestiary). Choose or create a
[fictitious creature](https://en.wikipedia.org/wiki/Hulder) for the subject matter and consider using content based on a
[real creature](https://en.wikipedia.org/wiki/Columbidae).

<!-- #### Tyger Tyger -->
<!-- Create a webpage 4 or more screens tall. On each of the 4 screens use type, -->
<!-- image, similarity, proximity, continuance, scale, texture, tone, color (black -->
<!-- and only one other color) to experiment with symmetry. Please come prepared to -->
<!-- explain your chosen use of symmetry and the design principles used to achieve it. -->

<!-- #### Illuminated Manuscript vs. The Machine -->
<!-- In the reading _Working With Style_ we examined the difference between modern -->
<!-- and classical page layout. Using an excerpt of your favorite short story or -->
<!-- essay, create two different layouts. One in a classical style and the other -->
<!-- modern. Use the same HTML code for both styles. -->

<!-- Bonus Points: Use a single HTML file but change between modern and classical based -->
<!-- on screen size or other event. -->

<!-- #### Type Specimen -->
<!-- Recreate Ellen Lupton's _Type Spec Demo_ as a web page. You may ignore the red -->
<!-- notes. Choose a typeface you wish to explore and create a web version of a type -->
<!-- specimen. Create a responsive web page beginning with the mobile version. -->

<!-- #### Broadside -->
<!-- #### Responsive Recipe -->
<!-- Using the recipe you created early, redesign the layout for accessibility and for viewing on multiple devices. Test your design for desktop, tablet, phone, touch and mouse. -->

<!-- This assignment adds another level of complexity to the _Recipe_ assignment. Students will become comfortable separating the meaning and intent of their work from its presentation. We will also talk about presenting to a diverse audience with differing needs. -->

<!-- #### Old Master Study -->
<!-- Choose a painting to be the subject of a composition study. Recreate the composition with code using basic shapes, lines, and colors. Display your work on a webpage with an image of the original work. Include information identifying the original work. -->

<!--     In this version of a standard art school assignment, students use code to explore the composition of the work of an old master. Students will have to use code to create and layer shapes but be limited in their expression by the requirement to replicate the composition of an older work. -->

<!-- #### Patterns -->
<!-- Create a webpage showing a series of 4 different patterns generated with code. -->

<!-- Code excels at repetition. Choose four words (e.g. winter, spring, summer, fall). Use variables and loops to generate a series of four patterns which illustrate those patterns. Display the patterns on a webpage. Each pattern should be labeled with its word. -->


<!-- Pattern making is a satisfying codemaking exercise that introduces students to the power programming. Students will present their code in class and work with their classmates to solve problems.  -->

    
<!-- #### Forking Paths -->
<!-- Create an interactive narrative. Allow your user choose their path through the experience and allow their decisions to affect the narrative programmatically. Use images, sound, video and type. Consider alternate forms of user input. -->


<!-- Students will have to research the programming concepts they wish to use. They will require creative thinking to create the experiences they wish to present. They will be required to use a variety of media elements. Students will work together to troubleshoot their code. -->

#### Final Project: Student's choice
Create a web site of your choosing for the final project. You proposal must be approved.
    
<!-- #### Final Project: FUTURE PRODUCT -->
<!-- Create *Product Page*, a page introducing a consumer product. Invent a product for you page that satisfies a future need. Ask yourself *how could society change in the future*. What needs will these changes bring and what product could satisfy those needs. The final project will be divided over 3 milestones and each will be graded individually. Students may submit an alternate final project of their choosing for approval. -->

The final project is designed to give the students the freedom to be creative and explore the aspects of the course that interests them while providing a structured framework that is of their own design. 
##### Final Project Proposal
Create a simple presentation pitching your final project to the class. Include a description of the project, an outline of the process you will use to complete it, and what you hope to achieve by presenting your work to the public.
<!-- #### UX Documentation & Development Plan -->
<!-- Turn in a PDF of the UX documentation for the final project. Include a site map and user journey. Also turn in a development accounting for a three week development period. Be specific about what you intend to have completed at each milestone. -->

## Required Reading / Media

[How to / Why Make Internet Art](http://netart.rocks)

[*What is Code?*, Paul Ford](http://www.bloomberg.com/graphics/2015-paul-ford-what-is-code/)

[*Paragraphs on Conceptual Art*, Sol Lewitt](https://www.dropbox.com/s/civ8ofgp13opp7v/Paragraphs_on_Conceptual_Art_Sol_LeWitt.pdf?dl=0)

[Guide to Website Navigation]( http://sixrevisions.com/user-interface/navigation-design-patterns/)

[Web Design: The First 100 Years, Maciej Cegłowski](http://idlewords.com/talks/web_design_first_100_years.htm)

*Toward Aesthetic Guidelines for Paintings with the Aid of a Computer*, Vera Molnar

*Hackers and Painters*, Paul Graham

*The Library of Babel*, Jorge Luis Borges

*The Garden of Forking Paths*, Jorge Luis Borges

## Materials and Supplies
-Access to your FTP server.

## Class Resources
[Resources file](https://gitlab.com/arielchuri/creativecomputing/blob/master/resources.md)

[W3 Schools](http://www.w3schools.com/)

[freeCodeCamp]( https://www.freecodecamp.org/challenges/say-hello-to-html-elements )

[Unix for Mac Users](https://www.lynda.com/Mac-OS-X-10-6-tutorials/Unix-for-Mac-OS-X-Users/78546-2.html)

[Git on Lynda.com](https://www.lynda.com/GitHub-training-tutorials/1384-0.html)

[Special Characters](http://www.w3schools.com/charsets/ref_utf_latin1_supplement.asp)

[CSS Tools: Reset CSS](http://meyerweb.com/eric/tools/css/reset/)

[Javascript Reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference)

[Get Started with the Google Fonts API](https://developers.google.com/fonts/docs/getting_started)

[Introduction to jQuery](https://www.edx.org/course/introduction-jquery-microsoft-dev208x-3)

[Interactivity with JavaScript | Coursera](https://www.coursera.org/learn/javascript#%20)

[HTML, CSS, and Javascript for Web Developers | Coursera](https://www.coursera.org/learn/html-css-javascript-for-web-developers)

[Full Stack Web and Multiplatform Mobile App Development](https://www.coursera.org/specializations/full-stack-mobile-app-development)

[Intro to JavaScript](https://www.udacity.com/course/intro-to-javascript--ud803)

[A-Frame](https://aframe.io)   

### FTP ( File Transfer Protocol )
You should have received access to a folder on the newschool servers from IT services. If not you will need to get that information from them. Inside this folder (also known as a directory) is another folder named *public_html*. This folder is viewable online at the address you have received.

To access this folder and upload your site:
Option 1: Use Cyberduck or other FTP program.
- Download an FTP program like Cyberduck.
- Set the mode to SFTP
- Input the correct name, password, and port number.
You can now upload files and folders.

Option 2: Use the Terminal
- Open the application _Terminal_ on your computer.
- Navigate to the folder containing your site's folder using the *cd* command. example: cd Documents/school/creative\ computing/mySiteFolder
- You can check the contents of this folder with *ls* (list). The folder containing your site should be in this list of files and folders.
- Next you will access the remote folder with _sftp_. Type the following into the terminal and press return.
    sftp -P 222 yourUserName@b.parsons.edu.
- After entering your password you will be in the remote server. *ls* to list the contents of a folder. *cd public_html* to move into that folder.
- Now create a folder to hold your site by typing _mkdir yourSiteFoler_
- Move into the folder you created with _cd yourSiteFoler_
- Now you can use the *put* command. *put -R mySiteFolder. Your site is now online. and available at the http://b.parsons.edu/~yourUserName/yourSiteFolderi*


### Code Editors:
+ [Bare Bones Software | BBEdit 11](http://www.barebones.com/products/bbedit/)
+ [Brackets - A modern, open source code editor that understands web design.](http://brackets.io/)
+ [Atom](https://atom.io/)
+ [Vim] (Already on your computer)
+ [Spacemacs] (http://spacemacs.org/)
+ [Nano] (Already on your computer)

## New School Resources

The university provides many resources to help students achieve academic and artistic excellence. These resources include:

- [The University and associated Libraries](http://library.newschool.edu)
- [The University Learning Center](http://www.newschool.edu/learning-center)

For additional help with coursework assigned during the semester, you are encouraged to schedule tutoring sessions at the University Learning Center (ULC). Individual appointments are offered in Writing (all levels), Math, Adobe, Computer Programming, Oral Presentations and Time Management. Sessions are interactive, with both tutor and student participating. Appointments can be scheduled on Starfish or you can stop by for a walk-in session. The ULC is located on the 6th floor of 66 West 12th street. Academic and skill-building workshops are also offered. For a complete list of services, workshops, and general information, visit www.newschool.edu/learning-center.

### [University Disabilities Service](http://www.newschool.edu/student-disability-services/)

In keeping with the university’s policy of providing equal access for students with disabilities, any student with a disability who needs academic accommodations is welcome to meet with me privately. All conversations will be kept confidential. Students requesting any accommodations will also need to contact Student Disability Service (SDS). SDS will conduct an intake and, if appropriate, the Director will provide an academic accommodation notification letter for you to bring to me. At that point, I will review the letter with you and discuss these accommodations in relation to this course.

### Making Center

Faculty who are planning curriculum that makes use of specific resources should contact the Making Center in
advance to coordinate.
The Making Center is a constellation of shops, labs, and open workspaces that are situated across the New
School to help students express their ideas in a variety of materials and methods. We have resources to help
support woodworking, metalworking, ceramics and pottery work, photography and film, textiles, printmaking,
3D printing, manual and CNC machining, and more. A staff of technicians and student workers provide expertise
and maintain the different shops and labs. Safety is a primary concern, so each area has policies for access,
training, and etiquette that students and faculty should be familiar with. Many areas require specific
orientations or trainings before access is granted. Detailed information about the resources available, as well as
schedules, trainings, and policies can be found at [resources.parsons.edu](http://resources.parsons.edu).
    
## Grading Standards
### Undergraduate
A student’s final grades and GPA are calculated using a 4.0 scale.

A  [4.0]
Work of exceptional quality, which often goes beyond the stated goals of the course 


A- [3.7]
Work of very high quality


B+ [3.3]
Work of high quality that indicates higher than average abilities


B  [3.0]
Very good work that satisfies the goals of the course


B- [2.7]
Good work


C+ [2.3]
Above-average work

C  [2.0]
Average work that indicates an understanding of the course material; passable 
Satisfactory completion of a course is considered to be a grade of C or higher.


C- [1.7]
Passing work but below good academic standing


D  [1.0]
Below-average work that indicates a student does not fully understand the assignments; 
Probation level though passing for credit

F  [0.0]
Failure, no credit

### Graduate

A    Work of exceptional quality 
A-    Work of high quality
B+    Very good work
B     Good work; satisfies course requirements 
Satisfactory completion of a course is considered to be a grade of B or higher.

B-    Below-average work
C+     Less than adequate work
C     Well below average work
C-    Poor work; lowest possible passing grade
F    Failure
GM    Grade missing for an individual

Grades of D are not used in graduate level courses.

Grade of W
The grade of W may be issued by the Office of the Registrar to a student who officially withdraws from a course within the applicable deadline. There is no academic penalty, but the grade will appear on the student transcript.


Grade of Z
This grade is to be assigned to students who have never attended or stopped attending classes.  Exceptions can be made if the student has completed enough work to warrant a grade (including a failing grade), and arrangements have been made with the instructor(s) and the Dean’s Office prior to grade submission.  The Z grade does not calculate into the student’s GPA.  

Grades of Incomplete 
The grade of I, or temporary incomplete, may be granted to a student under unusual and extenuating circumstances, such as when the student’s academic life is interrupted by a medical or personal emergency. This mark is not given automatically but only upon the student’s request and at the discretion of the instructor. A Request for Incomplete form must be completed and signed by the student and instructor. The time allowed for completion of the work and removal of the “I” mark will be set by the instructor with the following limitations: [You should include one of the following standards, depending on the level of your course].


Undergraduate students: Work must be completed no later than the seventh week of the following fall semester for spring or summer term incompletes and no later than the seventh week of the following spring semester for fall term incompletes. Grades of “I” not revised in the prescribed time will be recorded as a final grade of “F” by the Registrar’s Office. 

Graduate students: Work must be completed no later than one year following the end of the class. Grades of “I” not revised in the prescribed time will be recorded as a final grade of “N” by the Registrar’s Office. 

## College, School, Program and Class Policies

### Canvas
Use of Canvas may be an important resource for this class. Students should check it for announcements before coming to class each week.
 
### Electronic Devices 
The use of electronic devices (phones, tablets, laptops, cameras, etc.) is permitted when the device is being used in relation to the course's work. All other uses are prohibited in the classroom and devices should be turned off before class starts.

### Responsibility 
Students are responsible for all assignments, even if they are absent. Late assignments, failure to complete the assignments for class discussion and/or critique, and lack of preparedness for in-class discussions, presentations and/or critiques will jeopardize your successful completion of this course.  

### Active Participation and Attendance
Class participation is an essential part of class and includes: keeping up with reading, assignments, projects, contributing meaningfully to class discussions, active participation in group work, and coming to class regularly and on time.  

Parsons’ attendance guidelines were developed to encourage students’ success in all aspects of their academic programs. Full participation is essential to the successful completion of coursework and enhances the quality of the educational experience for all, particularly in courses where group work is integral; thus, Parsons promotes high levels of attendance. Students are expected to attend classes regularly and promptly and in compliance with the standards stated in this course syllabus. 

While attendance is just one aspect of active participation, absence from a significant portion of class time may prevent the successful attainment of course objectives. A significant portion of class time is generally defined as the equivalent of three weeks, or 20%, of class time. Lateness or early departure from class may be recorded as one full absence. Students may be asked to withdraw from a course if habitual absenteeism or tardiness has a negative impact on the class environment.

I will assess each student’s performance against all of the assessment criteria in determining your final grade.

### Academic Honesty and Integrity
Compromising your academic integrity may lead to serious consequences, including (but not limited to) one or more of the following: failure of the assignment, failure of the course, academic warning, disciplinary probation, suspension from the university, or dismissal from the university.

Students are responsible for understanding the University’s policy on academic honesty and integrity and must make use of proper citations of sources for writing papers, creating, presenting, and performing their work, taking examinations, and doing research. It is the responsibility of students to learn the procedures specific to their discipline for correctly and appropriately differentiating their own work from that of others. The full text of the policy, including adjudication procedures, is found at
http://www.newschool.edu/policies/# Resources regarding what plagiarism is and how to avoid it can be found on the Learning Center’s website: http://www.newschool.edu/university-learning-center/student-resources/

The New School views “academic honesty and integrity” as the duty of every member of an academic community to claim authorship for his or her own work and only for that work, and to recognize the contributions of others accurately and completely. This obligation is fundamental to the integrity of intellectual debate, and creative and academic pursuits. Academic honesty and integrity includes accurate use of quotations, as well as appropriate and explicit citation of sources in instances of paraphrasing and describing ideas, or reporting on research findings or any aspect of the work of others (including that of faculty members and other students). Academic dishonesty results from infractions of this “accurate use”. The standards of academic honesty and integrity, and citation of sources, apply to all forms of academic work, including submissions of drafts of final papers or projects. All members of the University community are expected to conduct themselves in accord with the standards of academic honesty and integrity. Please see the complete policy in the Parsons Catalog.

### Intellectual Property Rights
The New School (the "university") seeks to encourage creativity and invention among its faculty members and students. In doing so, the University affirms its traditional commitment to the personal ownership by its faculty members and students of Intellectual Property Rights in works they create. The complete policy governing Intellectual Property Rights may be seen on the [university website, on the Provost’s page.](https://www.newschool.edu/provost/accreditation-policies/)

### Communication Design Policies
A. Communication Design Zero Tolerance Attendance Policy
In order to foster a studio learning environment where we all learn from peers and through dialogue, timely and regular attendance is a strict expectation for all Communication Design students. Students who are not present in class are unable to meet the learning outcomes of a Communication Design course.

For classes meeting once a week, students are allowed 2 absences. For classes meeting twice a week, students are allowed 4 absences. Any absence beyond the allowed absences will result in an automatic failure (F) for the course. There are no excused absences. This applies to each and every student.

A student is deemed tardy if a student fails to arrive within 15 minutes past the beginning of class. 2 tardies will result in an automatic absence. A student who arrives an hour past the beginning of class will be deemed absent.


B. No Late Work and Missed Critiques
Work that is submitted past the assignment due date will result in an automatic failure for the assignment. For work presented in critique, absence at the critique will result in an automatic failure for the assignment. 


C. CD App
Information about upcoming CD Lectures, events from AIGA NY/TDC, the CD Library, and the CD Paper Store can be found on the CD App.

https://cdparsons.glideapp.io/

To install the app on your phone:

For iOS users: 
1. Visit the link in Safari, tap on the "Share" icon located at the bottom of the page
2. Tap the "Add to Home Screen" button
3. Tap the "Add" button at the top right corner of the screen

For Android users:
1. Tap on the notification banner at the bottom of the page (alternatively, you can tap the "Add to Home Screen" option inside the menu at the top right corner of the screen)
2. Tap the "Add" button on the modal
